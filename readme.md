# Bot do pozycjonowania stron

Bot wykorzystujący różne VPN'y (OpenVPN), Selenium oraz Pythona do nabijania odwiedzin - nie jest to idealne rozwiązanie.

* VPNControl.py - zarządzanie połączeniami VPN z wykorzystaniem OpenVPN
* botWWW.py - BOT symulujący zachowanie użytkownika

Screenshot przedstawia panel Google Analytics z okresu testowania bota - na nieistniejącej już, mojej stronie Antagonista.pl

![](readme_files/antagonista_googleanal.png)
