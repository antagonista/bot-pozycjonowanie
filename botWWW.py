# -*- coding: utf-8 -*-

# LAST VERSION

from selenium import webdriver
import random
import os
import time
from BeautifulSoup import BeautifulSoup
import re
import tldextract

class Bot():

    browserType = None
    browser = None
    # ["Firefox", "Chrome", "Opera"]
    browserList = ["Firefox", "Chrome"]
    sudoMode = None
    defaultGoogleSite = "https://www.google.pl/"
    defaultInputGoogle = "lst-ib"
    googlePage = 0
    randomTimeFrom = 3
    randomTimeFor = 5
    titleFoundedLinkInGoogle = None
    randomStepSiteFrom = 5
    randomStepSiteFor = 15
    bannedLinkList = None

    def __init__(self,browserType = None,sudoMode = False):
        self.browserType = browserType
        self.sudoMode = sudoMode
        self.InitiationBrowser()

    def __del__(self):

        if self.browserType == "Opera":
            os.system("killall opera-developer")
        else:
            self.browser.close()


    def InitiationBrowser(self):

        #random browser
        if (self.browserType == None):
            intRandomBrowser = random.randint(0,len(self.browserList)-1)
            self.browserType = self.browserList[intRandomBrowser]

        if self.browserType == "Firefox":
            self.browser = webdriver.Firefox()

        if self.browserType == "Chrome":
            if self.sudoMode == True:
                options = webdriver.ChromeOptions()
                options.add_argument("--no-sandbox")
                self.browser = webdriver.Chrome(chrome_options=options)
            else:
                self.browser = webdriver.Chrome()

        if self.browserType == "Opera":
            if self.sudoMode == True:
                options = webdriver.ChromeOptions()
                options.add_argument("--no-sandbox")
                self.browser = webdriver.Opera(options=options)
            else:
                self.browser = webdriver.Opera()

    def OpenGoogle(self,keywords):

        self.browser.get(self.defaultGoogleSite)

        time.sleep(1)

        input = self.browser.find_element_by_id(self.defaultInputGoogle)
        input.location_once_scrolled_into_view
        input.clear()
        input.send_keys(keywords.decode("utf-8"))

        time.sleep(1)

        input.submit()

    def WaitRandomTime(self):
        randomTime = random.randint(self.randomTimeFrom,self.randomTimeFor)
        time.sleep(randomTime)

    def SearchDomainOnGoogleSite(self,domainToSearch):


        bf = BeautifulSoup(self.browser.page_source)
        links = bf.findAll('a')

        founded = False

        for link in links:

            foundHref = re.findall("href",link.__str__())


            if len(foundHref) != 0:

                print link

                foundDomain = re.findall(domainToSearch.__str__(),link.__str__())

                if len(foundDomain) != 0:
                    if link.text != "Kopia":
                        founded = True
                        self.titleFoundedLinkInGoogle = link.text
                        break

        return founded

    def SearchInGoogle(self,keywords,domainToSearch):

        self.OpenGoogle(keywords)

        while True:

            self.googlePage += 1

            if self.titleFoundedLinkInGoogle != None:
                break

            self.WaitRandomTime()

            foundDomain = self.SearchDomainOnGoogleSite(domainToSearch)

            if foundDomain == True:
                #click in founded link
                link = self.browser.find_element_by_partial_link_text(self.titleFoundedLinkInGoogle)
                link.location_once_scrolled_into_view
                time.sleep(1)
                link.click()

            else:
                #next page click link
                nextPage = self.browser.find_element_by_id("pnnext")
                nextPage.location_once_scrolled_into_view
                time.sleep(1)
                nextPage.click()

    def CheckBanedLink(self,linkToCheck):

        banned = False

        for link in self.bannedLinkList:

            found = re.findall(link,linkToCheck)

            if len(found) != 0:
                banned = True
                break

        return banned

    def BotOnSite(self,siteUrl = None,bannedLink = None):

        if siteUrl != None:
            self.browser.get(siteUrl)

        self.bannedLinkList = bannedLink

        # get domain
        foundDomain = re.findall("http[s]?://.*/",self.browser.current_url.__str__())

        domain = tldextract.extract(self.browser.current_url).registered_domain

        howManyStep = random.randint(self.randomStepSiteFrom,self.randomStepSiteFor)

        for x in range(0,howManyStep):

            selectLink = self.browser.find_elements_by_tag_name('a')

            selectedLinks = []

            for link in selectLink:
                href = link.get_attribute('href')

                if href != None:



                    found = re.findall(domain,href)

                    if len(found) != 0:

                        if bannedLink != None:

                            if self.CheckBanedLink(href) == False:
                                selectedLinks.append(href)

                        else:
                            selectedLinks.append(href)

            randomLink = random.randint(0,len(selectedLinks)-1)

            link = selectedLinks[randomLink]
            self.browser.get(link)

            self.WaitRandomTime()



            """

            selectedLink = selectedLinks[randomLink]
            selectedLink.location_once_scrolled_into_view
            time.sleep(1)
            selectedLink.click()
            self.WaitRandomTime()
            
            bot = Bot()
            bot.SearchInGoogle("blog opiniotwórczy","antagonista.pl")
            bot.BotOnSite("https://www.antagonista.pl/",["KanalRSS","GaleriaLordMKK","../GaleriaLordMKK"])
            
            """

















