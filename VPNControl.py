import os
import random
import re
import socket
import struct
import fcntl
import requests
import time



class OpenVPNControl():

    dirToVPNConfig = "/etc/openvpn/ovpn_tcp/"
    dirToAuthFile = "/etc/openvpn/auth"
    defaultIP = None
    connected = False
    connectedVPNName = None
    listVPN = None
    IPAdressVPN = None
    interfaceName = None
    howManyTrayConnect = 3

    def __init__(self, interfaceName = "eth0"):
        self.interfaceName = interfaceName

    def Connect(self,nameCon):
        print "sudo openvpn --config " + self.dirToVPNConfig + str(nameCon) + " --auth-user-pass " + self.dirToAuthFile + " &"
        s = os.popen("sudo openvpn --config " + self.dirToVPNConfig + str(nameCon) + " --auth-user-pass " + self.dirToAuthFile + " &")

        initialization = False

        while True:

            line = s.readline()

            found = re.findall("Initialization Sequence Completed", line)

            if len(found) != 0:
                print "znaleziono"
                initialization = True
                break

        if initialization == True:

            print "sprawdzanie polaczenia"

            time.sleep(5)

            checkCon = self.CheckConnection()


            if checkCon != False:
                print "jest polaczenie: " + str(checkCon)

                self.connected = True
                self.connectedVPNName = nameCon

            else:
                print "nie polaczono"
                self.connected = False

        else:
            self.connected = False

        return self.connected



    def Disconnect(self):
        os.system("sudo killall openvpn")
        return True

    def CreateListVPN(self):
        s = os.popen("ls /etc/openvpn/ovpn_tcp/")
        lines = s.readlines()

        linesTemp = []

        for line in lines:
            line = line.replace("\n","")
            linesTemp.append(line)

        self.listVPN = linesTemp

        return True

    def GetListOfVPN(self):
        if self.listVPN == None:
            self.CreateListVPN()

        return self.listVPN

    def ConnectToRandomVPN(self):


        if self.listVPN == None:
            self.CreateListVPN()

        while True:

            if self.connected == True:
                self.Disconnect()

            print "proba polaczenia"

            whichVPN = random.randint(0,len(self.listVPN)-1)
            connected = self.Connect(self.listVPN[whichVPN])

            if connected == True:
                break

    def CreateListWorkedVPN(self):
        self.CreateListVPN()

        listOfWorkedVPN = []

        for vpn in self.listVPN:

            self.Disconnect()

            trayCon = self.Connect(vpn)

            if trayCon == True:
                listOfWorkedVPN.append(vpn)

        print listOfWorkedVPN

        fileHandle = open("listWorkedVPN","w").writelines(listOfWorkedVPN)




    def CheckConnection(self):

        try:
            ipAddr = requests.get("https://google.pl/").text
            return True

        except:
            return False












